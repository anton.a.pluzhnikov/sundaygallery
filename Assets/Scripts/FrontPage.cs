﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrontPage : MonoBehaviour
{
    [SerializeField] private MorphRectIntoRect morphAnimation;
    [SerializeField] private Transform container;
    [SerializeField] private Button closeButton;

    private RectTransform currentItem;

    private void Start()
    {
        closeButton.onClick.AddListener(ClosePage);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ClosePage();
        }
    }

    public void OpenPage()
    {
        gameObject.SetActive(true);
    }
    
    public void ShowClosely(RectTransform original)
    {
        HideCloseButton();
        
        currentItem = Instantiate(original, container);

        currentItem.sizeDelta = original.sizeDelta;
        currentItem.position = original.position;
        
        morphAnimation.AnimatedObject = currentItem;
        morphAnimation.Play()
            .Then(ShowCloseButton)
            .Then(() => { SetScreenOrientation(ScreenOrientation.AutoRotation); });
    }

    private void HideCloseButton()
    {
        closeButton.gameObject.SetActive(false);
    }

    private void ShowCloseButton()
    {
        closeButton.gameObject.SetActive(true);
    }

    private void SetScreenOrientation(ScreenOrientation screenOrientation)
    {
        Screen.orientation = screenOrientation;
    }

    public void ClosePage()
    {
        gameObject.SetActive(false);

        if (currentItem != null)
        {
            Destroy(currentItem.gameObject);
        }
        
        SetScreenOrientation(ScreenOrientation.Portrait);
    }
}
