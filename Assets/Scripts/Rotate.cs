﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField] private float anglePerTick = 30;
    [SerializeField] private float tickFrequency = 0.1f;
    [SerializeField] private Vector3 rotationNormal = Vector3.back;
    
    private void OnEnable()
    {
        StartCoroutine(Spin());
    }

    private void OnDisable()
    {
        StopCoroutine(Spin());
    }

    private IEnumerator Spin()
    {
        while (true)
        {
            transform.Rotate(rotationNormal, anglePerTick);
            yield return new WaitForSeconds(tickFrequency);
        }
    }
}
