﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class GalleryDownloadManager : MonoBehaviour
{
    [SerializeField] private string baseUrl;
    [SerializeField] private int maxImages;
    [SerializeField] private int columns;
    [SerializeField] private GridLayoutGroup gridLayoutGroup;
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private GalleryItem imageViewPrefab;
    [SerializeField] private FrontPage frontPage;

    private int currentImageIndex;

    private void Start()
    {
        SetupGrid();
        frontPage.ClosePage();
    }

    private void SetupGrid()
    {
        gridLayoutGroup.constraintCount = columns;

        var gridWidth = gridLayoutGroup.GetComponent<RectTransform>().rect.width;
        var cellSize = (gridWidth - gridLayoutGroup.padding.left - gridLayoutGroup.padding.right -
                        (columns - 1) * gridLayoutGroup.spacing.x) / columns;
        
        gridLayoutGroup.cellSize = new Vector2(cellSize, cellSize);
    }
    
    private void Update()
    {
        if (scrollRect.verticalNormalizedPosition <= 0 && currentImageIndex < maxImages)
        {
            ShowMoreImages();
        }
    }

    private void ShowMoreImages()
    {
        var moreImages = gridLayoutGroup.constraintCount;

        for (var index = 0; index < moreImages && currentImageIndex < maxImages; index++)
        {
            var newImage = Instantiate(imageViewPrefab, scrollRect.content);
            currentImageIndex++;
            newImage.InitClickCallback(item =>
            {
                if (item.DoneDownloading)
                {
                    ShowItemClosely(item);
                }
            });
            newImage.DownloadImage($"{baseUrl}{currentImageIndex}.jpg");
        }
    }

    private void ShowItemClosely(GalleryItem item)
    {
        frontPage.OpenPage();
        frontPage.ShowClosely(item.GetComponent<RectTransform>());
    }
}
