﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GalleryItem : MonoBehaviour
{
    [SerializeField] private RawImage targetRawImage;
    [SerializeField] private Image loadingImage;
    [SerializeField] private Button button;

    public bool DoneDownloading { get; private set; }

    private string url;
    private Action<GalleryItem> clickCallback;

    private void Start()
    {
        button.onClick.AddListener( () => clickCallback?.Invoke(this));
    }

    public void InitClickCallback(Action<GalleryItem> callBack)
    {
        clickCallback = callBack;
    }
    
    public void DownloadImage(string url)
    {
        this.url = url;
        StartCoroutine(LoadTexture());
    }

    private IEnumerator LoadTexture()
    {
        targetRawImage.color = Color.clear;
        loadingImage.gameObject.SetActive(true);

        yield return GetTextureByUrl();
        
        DoneDownloading = true;
        
        targetRawImage.color = Color.white;
        loadingImage.gameObject.SetActive(false);
    }

    private IEnumerator GetTextureByUrl()
    {
        var www = UnityWebRequestTexture.GetTexture(url);

        yield return www.SendWebRequest();

        if(www.isNetworkError || www.isHttpError) 
        {
            Debug.Log(www.error);
        }
        else 
        {
            targetRawImage.texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
        }
    }
}
