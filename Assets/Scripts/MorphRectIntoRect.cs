﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MorphRectIntoRect : LtAnimation
{
    [SerializeField] private RectTransform animatedObject;
    [SerializeField] private RectTransform morphInto;
    [SerializeField] private float duration;
    [SerializeField] private AnimationCurve animationCurve;
    [SerializeField] private bool ignoreTimeScale;
    [SerializeField] private bool playOnEnable;

    public RectTransform AnimatedObject
    {
        get => animatedObject;
        set => animatedObject = value;
    }

    public override float Duration => duration;
    
    private LTDescr currentAnimation;
    private Vector3 initialPosition;
    private Vector2 initialSizeDelta;

    private void OnEnable()
    {
        if (playOnEnable)
        {
            Play();
        }
    }
    
    public override IltAnimation Play()
    {
        CancelAnimation();
        
        ChangeAnchorsOnly(animatedObject, morphInto.anchorMin, morphInto.anchorMax);
        RememberInitialParams();
        
        currentAnimation = LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, duration)
            .setIgnoreTimeScale(ignoreTimeScale)
            .setOnComplete(() =>
            {
                currentAnimation = null;
                DoThen();
            });

        return this;
    }
    
    protected override void OnAnimationUpdate(float value)
    {
        base.OnAnimationUpdate(value);
        SetAnimatedValue(value);
    }

    private void SetAnimatedValue(float value)
    {
        var animatedValue = animationCurve.Evaluate(value);
        
        //change position
        animatedObject.position = initialPosition * (1 - animatedValue) + new Vector3( 
                                      morphInto.position.x + morphInto.rect.width * (0.5f - morphInto.pivot.x) * morphInto.lossyScale.x,
                                      morphInto.position.y + morphInto.rect.height * (0.5f - morphInto.pivot.y) * morphInto.lossyScale.y,
                                  morphInto.position.z) * animatedValue;
        
        //change size
        animatedObject.sizeDelta = initialSizeDelta * (1 - animatedValue) + morphInto.sizeDelta * animatedValue;
    }
    
    public override IltAnimation SetPose(float value)
    {
        OnAnimationUpdate(value);
        return base.SetPose(value);
    }

    private void CancelAnimation()
    {
        if (currentAnimation != null)
        {
            LeanTween.cancel(currentAnimation.uniqueId);
        }
    }

    private void RememberInitialParams()
    {
        initialSizeDelta = animatedObject.sizeDelta;
        initialPosition = animatedObject.position;
    }

    private void ChangeAnchorsOnly(RectTransform rectTransform, Vector2 newAnchorsMin, Vector2 newAnchorsMax)
    {
        var position = rectTransform.position;
        var initialSize = new Vector2(animatedObject.rect.width, animatedObject.rect.height);
        
        rectTransform.anchorMin = newAnchorsMin;
        rectTransform.anchorMax = newAnchorsMax;
                
        rectTransform.position = position;
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, initialSize.x);
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, initialSize.y);
    }

    private void OnDisable()
    {
        CancelAnimation();
    }

    private void OnDestroy()
    {
        CancelAnimation();
    }
}
