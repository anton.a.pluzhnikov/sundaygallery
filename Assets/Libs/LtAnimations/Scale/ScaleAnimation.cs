﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleAnimation : LtAnimation
{
    [SerializeField] private Transform animatedObject;
    [SerializeField] private float duration;
    [SerializeField] private AnimationCurve xSizeCurve;
    [SerializeField] private AnimationCurve ySizeCurve;
    [SerializeField] private int loopCount = 1;
    [SerializeField] private bool ignoreTimeScale;
    [SerializeField] private bool playOnEnable;

    public Transform AnimatedObject
    {
        get => animatedObject;
        set => animatedObject = value;
    }
    
    public AnimationCurve XSizeCurve
    {
        get => xSizeCurve;
        set => xSizeCurve = value;
    }

    public AnimationCurve YSizeCurve
    {
        get => ySizeCurve;
        set => ySizeCurve = value;
    }

    public override float Duration
    {
        get => duration;
        set => duration = value;
    }
    
    private void OnEnable()
    {
        if (playOnEnable)
        {
            Play();
        }
    }

    private LTDescr currentAnimation;

    public override IltAnimation Play()
    {
        if (animatedObject == null)
        {
            animatedObject = transform;
        }

        CancelAnimation();
        currentAnimation = LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, duration)
           .setIgnoreTimeScale(ignoreTimeScale)
           .setLoopCount(loopCount)
           .setOnComplete(() =>
           {
               currentAnimation = null;
               DoThen();
           });

        return this;
    }

    protected override void OnAnimationUpdate(float value)
    {
        base.OnAnimationUpdate(value);
        var size = new Vector3(xSizeCurve.Evaluate(value), ySizeCurve.Evaluate(value), 1);
        animatedObject.localScale = size;
    }

    public override IltAnimation SetPose(float value)
    {
        OnAnimationUpdate(value);
        return base.SetPose(value);
    }

    private void CancelAnimation()
    {
        if (currentAnimation != null)
        {
            LeanTween.cancel(gameObject, currentAnimation.uniqueId);
        }
    }

    private void OnDisable()
    {
        CancelAnimation();
    }

    private void OnDestroy()
    {
        CancelAnimation();
    }
}
