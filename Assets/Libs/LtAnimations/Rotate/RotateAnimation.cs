﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAnimation : LtAnimation
{
    [SerializeField] private Transform animatedObject;
    [SerializeField] private float duration;
    [SerializeField] private AnimationCurve rotationCurve;
    [SerializeField] private int loopCount = 1;
    [SerializeField] private bool ignoreTimeScale;
    [SerializeField] private bool playOnEnable = true;

    public override float Duration => duration;
    private Vector3 beforeAnimationRotation;
    
    private void OnEnable()
    {
        if (playOnEnable)
        {
            Play();
        }
    }

    public override IltAnimation Play()
    {
        beforeAnimationRotation = animatedObject.rotation.eulerAngles;
        CancelAnimation();
        LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, duration)
            .setIgnoreTimeScale(ignoreTimeScale)
            .setLoopCount(loopCount)
            .setOnComplete(DoThen);

        return this;
    }

    protected override void OnAnimationUpdate(float value)
    {
        base.OnAnimationUpdate(value);
        var zRotation = rotationCurve.Evaluate(value);
        animatedObject.rotation = Quaternion.Euler(new Vector3(beforeAnimationRotation.x, beforeAnimationRotation.y, zRotation));
    }

    public override IltAnimation SetPose(float value)
    {
        OnAnimationUpdate(value);
        return base.SetPose(value);
    }

    private void CancelAnimation()
    {
        LeanTween.cancel(gameObject);
        animatedObject.rotation = Quaternion.Euler(beforeAnimationRotation);
    }

    public override IltAnimation Cancel()
    {
        CancelAnimation();
        return base.Cancel();
    }

    private void OnDisable()
    {
        CancelAnimation();
    }

    private void OnDestroy()
    {
        CancelAnimation();
    }
}
