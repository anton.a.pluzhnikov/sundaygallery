﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoNothingAnimation : LtAnimation
{
    [SerializeField] private float duration;
    [SerializeField] private bool ignoreTimeScale;

    public override float Duration => duration;
    
    private LTDescr currentAnimation;
    
    public override IltAnimation Play()
    {
        CancelAnimation();
        currentAnimation = LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, duration)
            .setIgnoreTimeScale(ignoreTimeScale)
            .setOnComplete(DoThen);

        return this;
    }
    
    private void CancelAnimation()
    {
        base.Cancel();
        
        if (currentAnimation != null)
        {
            LeanTween.cancel(gameObject, currentAnimation.uniqueId);
        }
    }

    private void OnDisable()
    {
        CancelAnimation();
    }

    private void OnDestroy()
    {
        CancelAnimation();
    }
}
