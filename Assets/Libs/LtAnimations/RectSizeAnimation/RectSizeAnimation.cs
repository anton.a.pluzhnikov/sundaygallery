﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RectSizeAnimation : LtAnimation
{
    [SerializeField] private RectTransform animatedObject;
    [SerializeField] private float duration;
    [SerializeField] private Vector2 startSize;
    [SerializeField] private Vector2 endSize;
    [SerializeField] private AnimationCurve xSizeCurve;
    [SerializeField] private AnimationCurve ySizeCurve;
    [SerializeField] private bool ignoreTimeScale;
    [SerializeField] private bool playOnEnable;
    [SerializeField] private int loopCount = 1;
    
    public override float Duration => duration;
    
    private LTDescr currentAnimation;
    
    private void OnEnable()
    {
        if (playOnEnable)
        {
            Play();
        }
    }

    public override IltAnimation Play()
    {
        CancelAnimation();
        currentAnimation = LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, duration)
            .setIgnoreTimeScale(ignoreTimeScale)
            .setLoopCount(loopCount)
            .setOnComplete(() =>
            {
                currentAnimation = null;
                DoThen();
            });

        return this;
    }

    protected override void OnAnimationUpdate(float value)
    {
        base.OnAnimationUpdate(value);
        var sizeX = Mathf.Lerp(startSize.x, endSize.x, xSizeCurve.Evaluate(value));
        var sizeY = Mathf.Lerp(startSize.y, endSize.y, ySizeCurve.Evaluate(value));
        var size = new Vector2(sizeX, sizeY);
        animatedObject.sizeDelta = size;
    }

    public override IltAnimation SetPose(float value)
    {
        OnAnimationUpdate(value);
        return base.SetPose(value);
    }

    private void CancelAnimation()
    {
        if (currentAnimation != null)
        {
            LeanTween.cancel(currentAnimation.uniqueId);
        }
    }

    private void OnDisable()
    {
        CancelAnimation();
    }

    private void OnDestroy()
    {
        CancelAnimation();
    }
}
