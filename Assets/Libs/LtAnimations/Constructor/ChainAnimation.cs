﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class ChainAnimation : LtAnimation
{
    [SerializeField] private List<ChainPartSettings> settings = new List<ChainPartSettings>();
    [SerializeField] private bool playOnEnable;
    [FormerlySerializedAs("removeDisabled")] [SerializeField] private bool ignoreDisabled;

    public override float Duration => GetDuration();

    private void OnEnable()
    {
        if (playOnEnable)
        {
            Play();
        }
    }

    public override IltAnimation Play()
    {
        IltAnimation currentAnimation = null;

        foreach (var animSettings in settings)
        {
            var animation = animSettings.animation;
            
            if (currentAnimation == null)
            {
                if (ignoreDisabled && animation.gameObject.activeInHierarchy == false)
                {
                    animation.Play();
                }
                else
                {
                    currentAnimation = animation.Play();
                }
                
                continue;
            }

            if (ignoreDisabled && animation.gameObject.activeInHierarchy == false)
            {
                animation.Play();
            }
            else
            {
                currentAnimation = currentAnimation.ThenPlayFromKeyPoint(animation,
                    animSettings.timeNormalized, animSettings.startPoint);
            }
        }

        if (currentAnimation != null)
        {
            currentAnimation.Then(DoThen);
        }
        else
        {
            DoThen();
        }

        return this;
    }

    public override IltAnimation SetPose(float value)
    {
        var time = value * Duration;

        var animationStartTime = 0f;
        for (var index = 0; index < settings.Count; index++)
        {
            if (index > 0)
            {
                animationStartTime += settings[index - 1].animation.Duration * settings[index].timeNormalized;
            }
            
            var setting = settings[index];
            setting.animation.SetPose(Mathf.Clamp01((time - animationStartTime) / setting.animation.Duration));
        }

        return base.SetPose(value);
    }

    private float GetDuration()
    {
        var duration = 0f;
        for (var index = 0; index < settings.Count; index++)
        {
            if (index > 0)
            {
                duration += settings[index - 1].animation.Duration * settings[index].timeNormalized;
            }

            if (index == settings.Count - 1)
            {
                duration += settings[index].animation.Duration;
            }
        }

        return duration;
    }

    [Serializable]
    private class ChainPartSettings
    {
        [SerializeField] public LtAnimation animation;
        [SerializeField] public float startPoint;
        [SerializeField] public float timeNormalized;
    }
}
