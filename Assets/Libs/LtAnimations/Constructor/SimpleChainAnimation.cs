﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SimpleChainAnimation : LtAnimation
{
    [SerializeField] private List<LtAnimation> chain = new List<LtAnimation>();
    [SerializeField] private bool playOnEnable;
    [SerializeField] private bool ignoreTimeScale;
    [SerializeField] private int loopCount;
    
    public override float Duration => GetDuration();
    
    private void OnEnable()
    {
        if (playOnEnable)
        {
            Play();
        }
    }
    
    private LTDescr currentAnimation;
    
    public override IltAnimation Play()
    {
        CancelAnimation();
        currentAnimation = LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, Duration)
            .setIgnoreTimeScale(ignoreTimeScale)
            .setLoopCount(loopCount)
            .setOnComplete(() =>
            {
                currentAnimation = null;
                DoThen();
            });

        return this;
    }
    
    protected override void OnAnimationUpdate(float value)
    {
        base.OnAnimationUpdate(value);
        SetPose(value);
    }
    
    public override IltAnimation SetPose(float value)
    {
        var time = value * Duration;

        var animationStartTime = 0f;
        for (var index = 0; index < chain.Count; index++)
        {
            if (index > 0)
            {
                animationStartTime += chain[index - 1].Duration;
            }
            
            var anim = chain[index];

            if (anim.Duration > 0)
            {
                anim.SetPose(Mathf.Clamp01((time - animationStartTime) / anim.Duration));
            }
            else
            {
                anim.SetPose(1);
            }
            
        }

        return base.SetPose(value);
    }
    
    private float GetDuration()
    {
        return chain.Sum(x => x.Duration);
    }
    
    private void CancelAnimation()
    {
        base.Cancel();
        
        if (currentAnimation != null)
        {
            LeanTween.cancel(gameObject, currentAnimation.uniqueId);
        }
    }
    
    private void OnDisable()
    {
        CancelAnimation();
    }

    private void OnDestroy()
    {
        CancelAnimation();
    }
}
