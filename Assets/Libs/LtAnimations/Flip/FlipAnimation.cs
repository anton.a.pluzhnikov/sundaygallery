﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LtAnimations
{
    public class FlipAnimation : LtAnimation
    {
        [SerializeField] private RectTransform front;
        [SerializeField] private RectTransform back;
        [SerializeField] private AnimationCurve flipAnimationCurve = AnimationCurve.Linear(0, 1, 1, 1);
        [SerializeField] private float duration = 1;
        [SerializeField] private float midpointNormalized = 0.4f;
        [SerializeField] private bool ignoreTimeScale;

        [SerializeField] private FlipStatus currentStatus;

        public FlipStatus CurrentStatus 
        {
            get => currentStatus;
            private set => currentStatus = value;
        }

        private bool inited;
        private LTDescr currentAnimation;

        private void OnEnable()
        {
            if (inited)
                return;

            if (CurrentStatus == FlipStatus.FlippingToBack || CurrentStatus == FlipStatus.FlippingToFront)
            {
                Play();
            }
            
            if (CurrentStatus == FlipStatus.ShowingBack)
            {
                CurrentStatus = FlipStatus.FlippingToBack;
                SetPose(1);
                CurrentStatus = FlipStatus.ShowingBack;
            }

            if (CurrentStatus == FlipStatus.ShowingFront)
            {
                CurrentStatus = FlipStatus.FlippingToFront;
                SetPose(1);
                CurrentStatus = FlipStatus.ShowingFront;
            }
                
            inited = true;
        }

        public override IltAnimation Play()
        {
            if (currentAnimation != null)
            {
                return this;
            }
            
            CancelAnimation();

            if (CurrentStatus == FlipStatus.ShowingBack)
            {
                CurrentStatus = FlipStatus.FlippingToFront;
            }

            if (CurrentStatus == FlipStatus.ShowingFront)
            {
                CurrentStatus = FlipStatus.FlippingToBack;
            }
            
            currentAnimation = LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, duration)
                .setIgnoreTimeScale(ignoreTimeScale)
                .setOnComplete(() =>
                {
                    if (CurrentStatus == FlipStatus.FlippingToFront)
                    {
                        CurrentStatus = FlipStatus.ShowingFront;
                    }

                    if (CurrentStatus == FlipStatus.FlippingToBack)
                    {
                        CurrentStatus = FlipStatus.ShowingBack;
                    }

                    currentAnimation = null;
                    
                    DoThen();
                });

            return this;
        }

        protected override void OnAnimationUpdate(float value)
        {
            base.OnAnimationUpdate(value);
            
            var evaluatedValue = flipAnimationCurve.Evaluate(value);

            if (value < midpointNormalized)
            {
                var widthScale = evaluatedValue;
                var currentFrontSide = CurrentStatus == FlipStatus.FlippingToBack? front : back;
                var otherSide = CurrentStatus == FlipStatus.FlippingToBack? back : front;
                currentFrontSide.localScale = new Vector3(widthScale, 1, 1);
                currentFrontSide.gameObject.SetActive(true);
                otherSide.gameObject.SetActive(false);
            }
            else
            {
                var widthScale = evaluatedValue;
                var currentFrontSide = CurrentStatus == FlipStatus.FlippingToBack? back : front;
                var otherSide = CurrentStatus == FlipStatus.FlippingToBack? front : back;
                currentFrontSide.localScale = new Vector3(widthScale, 1, 1);
                currentFrontSide.gameObject.SetActive(true);
                otherSide.gameObject.SetActive(false);
            }
        }
        
        public override IltAnimation SetPose(float value)
        {
            OnAnimationUpdate(value);
            return base.SetPose(value);
        }

        private void CancelAnimation()
        {
            if (currentAnimation != null)
            {
                LeanTween.cancel(gameObject, currentAnimation.uniqueId);
            }
        }

        private void OnDisable()
        {
            CancelAnimation();
        }

        private void OnDestroy()
        {
            CancelAnimation();
        }
        
        public enum FlipStatus
        {
            ShowingFront = 0,
            FlippingToBack = 1,
            ShowingBack = 2,
            FlippingToFront = 3
        }
    }
}
