﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphicsAlphaAnimation : LtAnimation
{
    [SerializeField] private Graphic[] graphicsToAnimate;
    [SerializeField] private float duration;
    [SerializeField] private AnimationCurve alphaAnimationCurve;
    [SerializeField] private bool ignoreTimeScale;
    [SerializeField] private bool pingPong;
    [SerializeField] private bool playOnEnable;
    
    public override float Duration => duration;
    
    private void OnEnable()
    {
        if (playOnEnable)
        {
            Play();
        }
    }

    public override IltAnimation Play()
    {
        CancelAnimation();
        var ltAnimation = LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, duration)
            .setIgnoreTimeScale(ignoreTimeScale)
            .setOnComplete(DoThen);

        if (pingPong)
        {
            ltAnimation.setLoopPingPong();
        }

        return this;
    }

    protected override void OnAnimationUpdate(float value)
    {
        base.OnAnimationUpdate(value);
        foreach (var graphic in graphicsToAnimate)
        {
            var color = graphic.color;
            graphic.color = new Color(color.r, color.g, color.b, alphaAnimationCurve.Evaluate(value));
        }
    }

    public override IltAnimation SetPose(float value)
    {
        OnAnimationUpdate(value);
        return base.SetPose(value);
    }

    private void CancelAnimation()
    {
        LeanTween.cancel(gameObject);
    }

    private void OnDisable()
    {
        CancelAnimation();
    }

    private void OnDestroy()
    {
        CancelAnimation();
    }
}
