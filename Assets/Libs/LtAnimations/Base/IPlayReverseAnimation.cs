﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayReverseAnimation : IltAnimation
{
    IltAnimation PlayBack();
}
