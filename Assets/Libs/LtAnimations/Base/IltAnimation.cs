﻿using System;

public interface IltAnimation
{
    float Duration { get; }
    IltAnimation Play();
    IltAnimation PlayFromPoint(float startPoint);
    IltAnimation SetPose(float value);
    IltAnimation AndPlay(IltAnimation animation, float start = 0);
    IltAnimation ThenPlay(IltAnimation animation, float start = 0);
    IltAnimation ThenPlayFromKeyPoint(IltAnimation animation, float keyPoint, float start = -1);
    IltAnimation Then(Action action);
    IltAnimation ThenFromKeyPoint(Action action, float keyPoint);
    IltAnimation Cancel();
}
