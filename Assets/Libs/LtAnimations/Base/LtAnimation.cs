﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LtAnimation : MonoBehaviour, IltAnimation
{
    private List<KeyPoint> keyPoints = new List<KeyPoint>();
    private Action then;
    public virtual float Duration { get; set; } = 0;

    public virtual IltAnimation Play()
    {
        SetPose(0);
        OnAnimationUpdate(1);
        DoThen();
        return this;
    }

    public virtual IltAnimation PlayFromPoint(float startPoint)
    {
        SetPose(startPoint);
        OnAnimationUpdate(1);
        DoThen();
        return this;
    }

    public virtual IltAnimation SetPose(float value)
    {
        return this;
    }

    public virtual IltAnimation AndPlay(IltAnimation animation, float start = 0)
    {
        animation.SetPose(start);
        return animation.Play();
    }

    public virtual IltAnimation ThenPlay(IltAnimation animation, float start = 0)
    {
        animation.SetPose(start);
        then += () => { animation.Play(); };
        return animation;
    }

    public IltAnimation ThenPlayFromKeyPoint(IltAnimation animation,float keyPoint, float start = -1)
    {
        if (start >= 0 && start <= 1)
        {
            animation.SetPose(start);
        }
        
        keyPoints.Add(new KeyPoint(keyPoint, () => { animation.Play(); }));
        return animation;
    }

    public virtual IltAnimation Then(Action action)
    {
        then += action;
        return this;
    }

    public IltAnimation ThenFromKeyPoint(Action action, float keyPoint)
    {
        keyPoints.Add(new KeyPoint(keyPoint, action));
        return this;
    }

    public virtual IltAnimation Cancel()
    {
        then = null;
        keyPoints.Clear();
        return this;
    }

    protected void DoThen()
    {
        var copyOfThen = then;
        then = null;
        copyOfThen?.Invoke();
    }

    protected virtual void OnAnimationUpdate(float value)
    {
        for (var index = 0; index < keyPoints.Count; index++)
        {
            var keyPoint = keyPoints[index];
            if (keyPoint.TimeNormalized <= value)
            {
                keyPoint.Action?.Invoke();
                keyPoints.RemoveAt(index);
                index--;
            }
        }
    }

    private class KeyPoint
    {
        public float TimeNormalized { get; private set; }
        public Action Action { get; private set; }

        public KeyPoint(float timeNormalized, Action action)
        {
            TimeNormalized = timeNormalized;
            Action = action;
        }
    }
}
