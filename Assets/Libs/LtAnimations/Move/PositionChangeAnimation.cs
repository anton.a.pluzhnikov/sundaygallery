﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionChangeAnimation : LtAnimation
{
    [SerializeField] private RectTransform animatedObject;
    [SerializeField] private float duration;
    [SerializeField] private Vector2 startPosition;
    [SerializeField] private bool getStartPosition;
    [SerializeField] private Vector2 endPosition;
    [SerializeField] private AnimationCurve xPositionChangeCurve;
    [SerializeField] private AnimationCurve yPositionChangeCurve;
    [SerializeField] private int loopCount = 1;
    [SerializeField] private bool ignoreTimeScale;
    [SerializeField] private bool playOnEnable;
    
    public override float Duration => duration;

    private bool positionTaken;
    
    private void OnEnable()
    {
        if (getStartPosition && !positionTaken)
        {
            startPosition = animatedObject.anchoredPosition;
        }
        
        if (playOnEnable)
        {
            Play();
        }
    }

    private LTDescr currentAnimation;
    
    public override IltAnimation Play()
    {
        CancelAnimation();
        currentAnimation = LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, duration)
            .setIgnoreTimeScale(ignoreTimeScale)
            .setLoopCount(loopCount)
            .setOnComplete(DoThen);

        return this;
    }

    public override IltAnimation PlayFromPoint(float startPoint)
    {
        if(startPoint < 0 || startPoint > 1)
            throw new ArgumentException("Point should be in range [0, 1]");
        
        CancelAnimation();
        currentAnimation = LeanTween.value(gameObject, OnAnimationUpdate, startPoint, 1, duration * (1 - startPoint))
            .setIgnoreTimeScale(ignoreTimeScale)
            .setOnComplete(DoThen);
        
        return this;
    }

    protected override void OnAnimationUpdate(float value)
    {
        base.OnAnimationUpdate(value);
        var posX = endPosition.x * xPositionChangeCurve.Evaluate(value) + startPosition.x * (1 - xPositionChangeCurve.Evaluate(value));
        var posY = endPosition.y * yPositionChangeCurve.Evaluate(value) + startPosition.y * (1 - yPositionChangeCurve.Evaluate(value));
        animatedObject.anchoredPosition = new Vector2(posX, posY);
    }

    public override IltAnimation SetPose(float value)
    {
        OnAnimationUpdate(value);
        return base.SetPose(value);
    }

    private void CancelAnimation()
    {
        base.Cancel();
        
        if (currentAnimation != null)
        {
            LeanTween.cancel(gameObject, currentAnimation.uniqueId);
        }
    }

    private void OnDisable()
    {
        CancelAnimation();
    }

    private void OnDestroy()
    {
        CancelAnimation();
    }
}
