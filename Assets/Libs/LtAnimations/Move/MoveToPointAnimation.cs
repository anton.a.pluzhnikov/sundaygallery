﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToPointAnimation : LtAnimation
{
    [SerializeField] private Transform animatedObject;
    [SerializeField] private float duration = 1;
    [SerializeField] private AnimationCurve xPositionCurve = AnimationCurve.Linear(0, 1, 1, 1);
    [SerializeField] private AnimationCurve yPositionCurve = AnimationCurve.Linear(0, 1, 1, 1);
    [SerializeField] private AnimationCurve zPositionCurve = AnimationCurve.Linear(0, 1, 1, 1);
    [SerializeField] private int loopCount = 1;
    [SerializeField] private bool ignoreTimeScale;
    [SerializeField] private bool playOnEnable;
    [SerializeField] private Vector3 startPosition;
    [SerializeField] private Vector3 endPosition;
    [SerializeField] private bool localSpace = true;

    public Transform AnimatedObject
    {
        get => animatedObject;
        set => animatedObject = value;
    }
    
    public AnimationCurve XPositionCurve
    {
        get => xPositionCurve;
        set => xPositionCurve = value;
    }

    public AnimationCurve YPositionCurve
    {
        get => yPositionCurve;
        set => yPositionCurve = value;
    }
    
    public AnimationCurve ZPositionCurve
    {
        get => zPositionCurve;
        set => zPositionCurve = value;
    }

    public override float Duration
    {
        get => duration;
        set => duration = value;
    }
    
    private void OnEnable()
    {
        if (playOnEnable)
        {
            Play();
        }
    }

    public Vector3 StartPosition
    {
        get => startPosition;
        set => startPosition = value;
    }
    
    public Vector3 EndPosition
    {
        get => endPosition;
        set => endPosition = value;
    }

    public bool LocalSpace
    {
        get => localSpace;
        set => localSpace = value;
    }
    
    private LTDescr currentAnimation;

    public override IltAnimation Play()
    {
        if (animatedObject == null)
        {
            animatedObject = transform;
        }
        
        CancelAnimation();
        currentAnimation = LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, duration)
            .setIgnoreTimeScale(ignoreTimeScale)
            .setLoopCount(loopCount)
            .setOnComplete(() =>
            {
                currentAnimation = null;
                DoThen();
            });

        return this;
    }

    protected override void OnAnimationUpdate(float value)
    {
        base.OnAnimationUpdate(value);

        var xWeight = xPositionCurve.Evaluate(value);
        var yWeight = yPositionCurve.Evaluate(value);
        var zWeight = zPositionCurve.Evaluate(value);
        
        var position = new Vector3(
            endPosition.x * xWeight + startPosition.x * (1 - xWeight),
            endPosition.y * yWeight + startPosition.y * (1 - yWeight),
            endPosition.z * yWeight + startPosition.z * (1 - yWeight));

        if (localSpace)
        {
            animatedObject.localPosition = position;
        }
        else
        {
            animatedObject.position = position;
        }
        
    }

    public override IltAnimation SetPose(float value)
    {
        OnAnimationUpdate(value);
        return base.SetPose(value);
    }

    private void CancelAnimation()
    {
        base.Cancel();
        
        if (currentAnimation != null)
        {
            LeanTween.cancel(gameObject, currentAnimation.uniqueId);
        }
    }

    private void OnDisable()
    {
        CancelAnimation();
    }

    private void OnDestroy()
    {
        CancelAnimation();
    }
}
