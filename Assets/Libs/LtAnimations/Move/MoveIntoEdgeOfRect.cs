﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveIntoEdgeOfRect : LtAnimation, IPlayReverseAnimation
{
    [SerializeField] private RectTransform animatedObject;
    [SerializeField] private RectTransform edgeContainer;
    [SerializeField] private float duration;
    [SerializeField] private AnimationCurve animationCurve;
    [SerializeField] private bool ignoreTimeScale;
    [SerializeField] private bool playOnEnable;
    [SerializeField] private Edge edge;
    public override float Duration => duration;

    private LTDescr currentAnimation;
    private Vector3 panelOutPosition;
    private Vector3 panelInPosition;
    
    private void OnEnable()
    {
        if (playOnEnable)
        {
            Play();
        }
    }
    
    public override IltAnimation Play()
    {
        CancelAnimation();
        CalculateKeyPositions();
        
        currentAnimation = LeanTween.value(gameObject, OnAnimationUpdate, 0, 1, duration)
            .setIgnoreTimeScale(ignoreTimeScale)
            .setOnComplete(() =>
            {
                currentAnimation = null;
                DoThen();
            });

        return this;
    }
    
    protected override void OnAnimationUpdate(float value)
    {
        base.OnAnimationUpdate(value);
        SetAnimatedValue(value);
    }

    private void SetAnimatedValue(float value)
    {
        var animatedValue = animationCurve.Evaluate(value);
        animatedObject.position = panelOutPosition * (1 - animatedValue) + panelInPosition * animatedValue;
    }
    
    public override IltAnimation SetPose(float value)
    {
        OnAnimationUpdate(value);
        return base.SetPose(value);
    }

    private void CancelAnimation()
    {
        if (currentAnimation != null)
        {
            LeanTween.cancel(currentAnimation.uniqueId);
        }
    }

    public IltAnimation PlayBack()
    {
        CancelAnimation();
        CalculateKeyPositions();
        
        currentAnimation = LeanTween.value(gameObject, OnReverseAnimationUpdate, 0, 1, duration)
            .setIgnoreTimeScale(ignoreTimeScale)
            .setOnComplete(() =>
            {
                currentAnimation = null;
                DoThen();
            });

        return this;
    }

    private void OnReverseAnimationUpdate(float value)
    {
        base.OnAnimationUpdate(value);
        SetAnimatedValue(1 - value);
    }

    private void CalculateKeyPositions()
    {
        switch (edge)
        {
            case Edge.Left:
                var panelOutX = edgeContainer.position.x - edgeContainer.rect.width * edgeContainer.lossyScale.x * edgeContainer.pivot.x -
                                animatedObject.rect.width * animatedObject.lossyScale.x * (1 - animatedObject.pivot.x);
                var panelInX = edgeContainer.position.x - edgeContainer.rect.width * edgeContainer.lossyScale.x * edgeContainer.pivot.x +
                               animatedObject.rect.width * animatedObject.lossyScale.x * animatedObject.pivot.x;

                panelOutPosition = new Vector3(panelOutX, animatedObject.position.y, animatedObject.position.z);
                panelInPosition = new Vector3(panelInX, animatedObject.position.y, animatedObject.position.z);
                
                break;
            case Edge.Right:
                throw new NotImplementedException();
                break;
            case Edge.Up:
                throw new NotImplementedException();
                break;
            case Edge.Down:
                throw new NotImplementedException();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    
    private void OnDisable()
    {
        CancelAnimation();
    }

    private void OnDestroy()
    {
        CancelAnimation();
    }

    private enum Edge
    {
        Left = 0,
        Right = 1,
        Up = 2,
        Down = 3,
    }
}
